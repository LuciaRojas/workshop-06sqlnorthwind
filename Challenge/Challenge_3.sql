/*
Challenge #3
Recupere la lista de los 10 productos más ordenados (order_details),
y la cantidad total de unidades ordenadas para cada uno de los
productos.
Challenge #3
Deberá incluir como mínimo los campos de código (product_code),
nombre del producto (product_name) y la cantidad de unidades.
*/
DESC order_details;
DESC products;

SELECT p.product_name, sum(od.quantity) as 'Cantidad'
FROM order_details od
JOIN products p
ON p.id = od.product_id
GROUP BY od.product_id
ORDER BY sum(od.quantity) DESC
LIMIT 10;
